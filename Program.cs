﻿using System;

namespace Triangle
{
    class Program
    {
        static void Main(string[] args)
        {
            TriangleClass tc = new TriangleClass(){
                Character = '*',
                Rows = 7,
                Columns = 14
            };

            System.Console.WriteLine(tc.Character + " " + tc.Rows + " "  + tc.Columns);
            tc.DrawEmpty();
        }
    }
}
