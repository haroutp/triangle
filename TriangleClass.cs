using System;

namespace Triangle
{
    class TriangleClass
    {
        private int rows;
        private int columns;
        private char character;

        public int Rows { get => rows; set => rows = value; }
        public int Columns { get => columns; set => columns = value; }
        public char Character { get => character; set => character = value; }

        public void DrawFull(){
            
            for (int i = 1; i <= Rows; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    System.Console.Write(Character);
                }
                System.Console.WriteLine();
            }
        }

        public void DrawEmpty(){
            for (int i = 1; i <= Rows; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    if(i == 0 || i == Rows){
                        System.Console.Write(Character);
                        continue;
                    }
                    if(j == 1 || j == i){
                        System.Console.Write(Character);
                    }else{
                        System.Console.Write(" ");
                    }
                }
                System.Console.WriteLine();
            }
        }
    }
}
